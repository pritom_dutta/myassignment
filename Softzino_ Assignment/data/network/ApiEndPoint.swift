//
//  ApiEndPoint.swift
//  Softzino_ Assignment
//
//  Created by Pritom Dutta on 1/9/21.
//

import Foundation
struct ApiEndPoint {
    public var API: String  = "http://easyservice.xyz/assign/"
    
    //Headers
    public var KEY_CONTENT_TYPE: String = "Content-Type";
    public var KEY_ACCEPT: String = "Accept";
    public var KEY_AUTHORIZATION:String = "Authorization";

    //Header Values
    public var VALUE_JSON: String = "application/json";
    public var VALUE_BEARER: String = "Bearer ";
    public var VALUE_MULTI_PART: String = "multipart/form-data";
}
