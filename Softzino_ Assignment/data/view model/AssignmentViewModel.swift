//
//  AssignmentViewModel.swift
//  Softzino_ Assignment
//
//  Created by Pritom Dutta on 1/9/21.
//

import Foundation
import Alamofire
import AlamofireImage

class AssignmentViewModel {
    
    private var mAssignmentParseApi = AssignmentParseApi()
    private var mAssignmentResponse = [AssignmentResponse]()
    
    //  MARK:- Get Assignment
    func getAssignmentData(completion: @escaping (Bool, String) -> ()) {
        mAssignmentParseApi.getAssignmentData{ [weak self](result) in
            switch result {
            case .success(let listOf):
                self?.mAssignmentResponse = listOf
                print(self?.mAssignmentResponse as Any)
                completion(true,"")
            case .failure(let error):
                completion(false,"\(error.localizedDescription)")
                print("Error proccessing data \(error.localizedDescription)")
            }
        }
    }
    
    func numberOfRowInSection() -> Int {
        if(self.mAssignmentResponse.count != 0){
            return self.mAssignmentResponse.count
        }
        return 0
    }
    func getData(index: IndexPath) -> AssignmentResponse {
        return (self.mAssignmentResponse[index.row])
    }
    
    func setImag(imageView: UIImageView,imgUrl: String) {
        let urlStr = imgUrl.replacingOccurrences(of: "\\", with: "//")
        let downloader = ImageDownloader()
        let urlRequest = URLRequest(url: URL(string: urlStr)!)
        downloader.download(urlRequest, completion:  { response in
            if case .success(let image) = response.result {
                imageView.image = image
            }
        })
    }
}
