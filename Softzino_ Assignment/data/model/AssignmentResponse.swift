//
//  AssignmentResponse.swift
//  Softzino_ Assignment
//
//  Created by Pritom Dutta on 1/9/21.
//

import Foundation

struct AssignmentResponse: Codable {
    let code: Int?
    let store_no,store_name,zone_name,region_name,store_photo_url,shopping_center_name: String?
    let address_details: AddressDetailsModel?
    var open_hours: [String:OpenHoursModel]?
}

struct OpenHoursModel: Codable {
    let starting_time,closing_time,notes,weekName: String?
}

struct AddressDetailsModel: Codable {
    let address_line,city_name,state_name,country_name,zip_code: String?
}

struct ReviewModel {
    let title,des: String?
}
