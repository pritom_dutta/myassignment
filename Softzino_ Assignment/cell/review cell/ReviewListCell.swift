//
//  ReviewListCell.swift
//  Softzino_ Assignment
//
//  Created by Pritom Dutta on 1/9/21.
//

import UIKit

class ReviewListCell: UICollectionViewCell {

    @IBOutlet weak var reviewWidthCons: NSLayoutConstraint!
    
    @IBOutlet weak var txtDes: UILabel!
    @IBOutlet weak var txtName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        let screenWidth = UIScreen.main.bounds.size.width
        reviewWidthCons.constant = screenWidth - 10
    }

}
