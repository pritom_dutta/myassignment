//
//  StoreListCell.swift
//  Softzino_ Assignment
//
//  Created by Pritom Dutta on 1/9/21.
//

import UIKit

class StoreListCell: UICollectionViewCell {

    @IBOutlet weak var shopListwidthCons: NSLayoutConstraint!
    @IBOutlet weak var txtAddressOne: UILabel!
    @IBOutlet weak var txtShopName: UILabel!
    @IBOutlet weak var imgShop: UIImageView!
    @IBOutlet weak var txtAddressTwo: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        let screenWidth = UIScreen.main.bounds.size.width
        shopListwidthCons.constant = screenWidth - 10
    }

}
