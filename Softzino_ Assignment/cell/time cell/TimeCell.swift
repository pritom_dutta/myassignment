//
//  TimeCell.swift
//  Softzino_ Assignment
//
//  Created by Pritom Dutta on 1/9/21.
//

import UIKit

class TimeCell: UICollectionViewCell {

    @IBOutlet weak var contsWidth: NSLayoutConstraint!
    
    @IBOutlet weak var txtTime: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        let screenWidth = UIScreen.main.bounds.size.width
        contsWidth.constant = screenWidth - 20
    }

}
