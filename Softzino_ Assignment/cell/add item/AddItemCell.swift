//
//  AddItemCell.swift
//  Softzino_ Assignment
//
//  Created by Pritom Dutta on 1/9/21.
//

import UIKit

class AddItemCell: UICollectionViewCell {

    @IBOutlet weak var txtTitle: UILabel!
    @IBOutlet weak var addItemWidthCons: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        let screenWidth = UIScreen.main.bounds.size.width
        addItemWidthCons.constant = screenWidth - 10
    }

}
