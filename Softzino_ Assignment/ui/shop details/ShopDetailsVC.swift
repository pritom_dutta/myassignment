//
//  ShopDetailsVC.swift
//  Softzino_ Assignment
//
//  Created by Pritom Dutta on 1/9/21.
//

import UIKit

class ShopDetailsVC: UIViewController {
    
    let cellId: String = "TimeCell"
    let cellIdTwo: String = "AddItemCell"
    let cellIdThree: String = "ReviewListCell"
    
    @IBOutlet weak var reviewHeigt: NSLayoutConstraint!
    @IBOutlet weak var openingHeight: NSLayoutConstraint!
    @IBOutlet weak var openingHoursCollectionView: UICollectionView!
    @IBOutlet weak var reviewCollectionView: UICollectionView!
    
    @IBOutlet weak var imgShop: UIImageView!
    @IBOutlet weak var txtShopName: UILabel!
    @IBOutlet weak var txtAddress: UILabel!
    @IBOutlet weak var txtContact: UILabel!
    @IBOutlet weak var txtName: UILabel!
    
    
    var mReviewModel = [ReviewModel]()
    var mOpeningModel = [OpenHoursModel]()
    
    var mAssignmentResponse: AssignmentResponse?
    let mAssignmentViewModel = AssignmentViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let nibCell = UINib(nibName: cellId, bundle: nil)
        self.openingHoursCollectionView.register(nibCell, forCellWithReuseIdentifier: cellId)
        
        let nibCellTwo = UINib(nibName: cellIdTwo, bundle: nil)
        self.openingHoursCollectionView.register(nibCellTwo, forCellWithReuseIdentifier: cellIdTwo)
        
        let nibCellThree = UINib(nibName: cellIdThree, bundle: nil)
        self.reviewCollectionView.register(nibCellThree, forCellWithReuseIdentifier: cellIdThree)
        self.reviewCollectionView.register(nibCellTwo, forCellWithReuseIdentifier: cellIdTwo)
        
        NotificationCenter.default.addObserver(self, selector: #selector(updateReview), name:NSNotification.Name(rawValue: "update_review"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(updateOpening), name:NSNotification.Name(rawValue: "update_opening"), object: nil)
        
        if mAssignmentResponse != nil{
            if let imgUrl = mAssignmentResponse?.store_photo_url{
                if imgUrl != ""{
                    mAssignmentViewModel.setImag(imageView: imgShop, imgUrl: imgUrl)
                }
            }
            txtShopName.text = mAssignmentResponse?.store_name ?? ""
            txtName.text = mAssignmentResponse?.store_name ?? ""
            txtAddress.text = "\(mAssignmentResponse?.address_details?.state_name ?? ""), \(mAssignmentResponse?.address_details?.country_name ?? "")"
            txtContact.text = "\(mAssignmentResponse?.zone_name ?? ""), \(mAssignmentResponse?.shopping_center_name ?? "")"
            
            for n in 1...(mAssignmentResponse?.open_hours!.count)! {
                let data  = mAssignmentResponse?.open_hours?["\(n)"]
                mOpeningModel.append(OpenHoursModel(starting_time: data?.starting_time ?? "", closing_time: data?.closing_time ?? "", notes: "", weekName: data?.weekName ?? ""))
            }
            
            self.openingHoursCollectionView.layoutIfNeeded()
            let height = (self.openingHoursCollectionView.collectionViewLayout.collectionViewContentSize.height) + 2*10
            openingHeight.constant = CGFloat(height)
            
            self.reviewCollectionView.layoutIfNeeded()
            let heightTwo = (self.reviewCollectionView.collectionViewLayout.collectionViewContentSize.height) + 2*10
            reviewHeigt.constant = CGFloat(heightTwo)
        }
        
    }

    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func updateReview(notification: NSNotification){
        if let data = notification.object as? ReviewModel{
            mReviewModel.append(data)
            self.reviewCollectionView.reloadData()
            self.reviewCollectionView.layoutIfNeeded()
            let heightTwo = (self.reviewCollectionView.collectionViewLayout.collectionViewContentSize.height) + 2*10
            reviewHeigt.constant = CGFloat(heightTwo)
        }
    }
    
    @objc func updateOpening(notification: NSNotification){
        if let data = notification.object as? OpenHoursModel{
            print(data.weekName ?? "")
            mOpeningModel.append(OpenHoursModel(starting_time: data.starting_time ?? "", closing_time: data.closing_time ?? "", notes: "", weekName: data.weekName ?? ""))
            self.openingHoursCollectionView.reloadData()
            self.openingHoursCollectionView.layoutIfNeeded()
            let heightTwo = (self.openingHoursCollectionView.collectionViewLayout.collectionViewContentSize.height) + 2*10
            openingHeight.constant = CGFloat(heightTwo)
        }
    }
    
    
}

extension ShopDetailsVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == openingHoursCollectionView{
            if indexPath.row == mOpeningModel.count{
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "AddOpeningHours") as! AddOpeningHours
                self.navigationController?.isToolbarHidden = true
                self.navigationController?.isNavigationBarHidden = true
                self.navigationController?.pushViewController(vc, animated:   true)
            }
        }else{
            if indexPath.row == mReviewModel.count{
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "ReviewVC") as! ReviewVC
                self.navigationController?.isToolbarHidden = true
                self.navigationController?.isNavigationBarHidden = true
                self.navigationController?.pushViewController(vc, animated:   true)
            }
        }
        
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == openingHoursCollectionView{
            return  mOpeningModel.count + 1
        }
        return mReviewModel.count + 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == openingHoursCollectionView{
            if indexPath.row == mOpeningModel.count {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdTwo, for: indexPath) as! AddItemCell
                cell.txtTitle.text = "Add More Opening Hours"
                return cell
            }
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! TimeCell
            let data  = mOpeningModel[indexPath.row]
            cell.txtTime.text = "\(data.weekName ?? "") > \(data.starting_time ?? "") - \(data.closing_time ?? "")"
            return cell
        }
        else{
            if indexPath.row == mReviewModel.count{
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdTwo, for: indexPath) as! AddItemCell
                cell.txtTitle.text  = "Add Your Review"
                return cell
            }
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdThree, for: indexPath) as! ReviewListCell
            cell.txtName.text = mReviewModel[indexPath.row].title ?? ""
            cell.txtDes.text = mReviewModel[indexPath.row].des ?? ""
            return cell
        }
       
        
    }
    
}
