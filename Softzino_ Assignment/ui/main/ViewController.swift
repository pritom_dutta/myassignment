//
//  ViewController.swift
//  Softzino_ Assignment
//
//  Created by Pritom Dutta on 1/9/21.
//

import UIKit

class ViewController: UIViewController {
    
    let cellId: String = "StoreListCell"
    @IBOutlet weak var shopCollectionView: UICollectionView!
    
    let mAssignmentViewModel = AssignmentViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let nibCell = UINib(nibName: cellId, bundle: nil)
        self.shopCollectionView.register(nibCell, forCellWithReuseIdentifier: cellId)
        
        showSpinner(onView: self.view)
        mAssignmentViewModel.getAssignmentData(){
            [self] (flag,result) in
            self.shopCollectionView.reloadData()
            if flag{
                self.shopCollectionView.reloadData()
            }else{
                DispatchQueue.main.async {
                    self.view.makeToast("\(result)")
                }
            }
            DispatchQueue.main.async {
                self.removeSpinner()
            }
        }
        
    }
}


extension ViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "ShopDetailsVC") as! ShopDetailsVC
        vc.mAssignmentResponse = mAssignmentViewModel.getData(index: indexPath)
        self.navigationController?.isToolbarHidden = true
        self.navigationController?.isNavigationBarHidden = true
        self.navigationController?.pushViewController(vc, animated:   true)
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return mAssignmentViewModel.numberOfRowInSection()
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! StoreListCell
        let data = mAssignmentViewModel.getData(index: indexPath)
        if let imgUrl = data.store_photo_url{
            if imgUrl != ""{
                mAssignmentViewModel.setImag(imageView: cell.imgShop, imgUrl: imgUrl)
            }
        }
        cell.txtShopName.text = data.store_name ?? ""
        cell.txtAddressOne.text = "\(data.address_details?.state_name ?? ""), \(data.address_details?.country_name ?? "")"
        cell.txtAddressTwo.text = "\(data.zone_name ?? ""), \(data.shopping_center_name ?? "")"
        return cell
        
    }
    
}
