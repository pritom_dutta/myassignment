//
//  ReviewVC.swift
//  Softzino_ Assignment
//
//  Created by Pritom Dutta on 1/9/21.
//

import UIKit
import KMPlaceholderTextView

class ReviewVC: UIViewController {

    @IBOutlet weak var saveBtn: UIButton!
    @IBOutlet weak var etName: UITextField!
    @IBOutlet weak var etDetails: KMPlaceholderTextView!
    var mReviewModel: ReviewModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        hideKeyboardWhenTappedAround()
        
        etName.clipsToBounds = true
        etName.layer.cornerRadius = 5
        etName.layer.borderWidth = 1
        etName.layer.borderColor = UIColor.gray.cgColor
        
        etDetails.clipsToBounds = true
        etDetails.layer.cornerRadius = 5
        etDetails.layer.borderWidth = 1
        etDetails.layer.borderColor = UIColor.gray.cgColor
        
        saveBtn.clipsToBounds = true
        saveBtn.layer.cornerRadius = 10
    }
    
    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }

    @IBAction func btnSave(_ sender: Any) {
        if etName.text == ""{
            self.view.makeToast("Enter Your Name")
            return
        }
        
        if etDetails.text == ""{
            self.view.makeToast("Enter Some Details")
            return
        }
        
        mReviewModel = ReviewModel(title: etName.text ?? "", des: etDetails.text ?? "")
        
        if mReviewModel != nil{
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "update_review"), object: mReviewModel)
            self.navigationController?.popViewController(animated: true)
        }
        
        
    }
}
