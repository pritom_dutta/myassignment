//
//  AddOpeningHours.swift
//  Softzino_ Assignment
//
//  Created by Pritom Dutta on 1/9/21.
//

import UIKit
import DropDown

class AddOpeningHours: UIViewController {
    
    let weekList = ["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"]
    let drop = DropDown()
    
    @IBOutlet weak var saveBtn: UIButton!
    @IBOutlet weak var etWeek: UITextField!
    @IBOutlet weak var etFromDate: UITextField!
    @IBOutlet weak var etEndDate: UITextField!
    
    var mModel: OpenHoursModel?
    
    var isSelect = false
    override func viewDidLoad() {
        super.viewDidLoad()
        saveBtn.clipsToBounds = true
        saveBtn.layer.cornerRadius = 10
    }
    
    
    @IBAction func btnWeek(_ sender: Any) {
        drop.dataSource = weekList
        drop.anchorView = (sender as! AnchorView) //5
        drop.bottomOffset = CGPoint(x: 0, y: (sender as AnyObject).frame.size.height) //6
        drop.show() //7
        drop.selectionAction = { [weak self] (index: Int, item: String) in //8
            guard let _ = self else { return }
            self!.etWeek.text = item
        }
    }
    
    @IBAction func btnEndDate(_ sender: Any) {
        isSelect = false
        showPicker()
    }
    
    @IBAction func btnFromDate(_ sender: Any) {
        isSelect = true
        showPicker()
    }
    
    @objc func showPicker() {
        let timeSelector = TimeSelector()
        timeSelector.timeSelected = {
            (timeSelector) in
            self.setLabelFromDate(timeSelector.date)
        }

                
        let date = Date()
        let calendar = Calendar.current
        let hour = calendar.component(.hour, from: date)
        let min = calendar.component(.minute, from: date)
        //let min = calendar.component(.minute, from: date)

        timeSelector.overlayAlpha = 0.8
        timeSelector.clockTint = timeSelector_rgb(0, 230, 0)
        timeSelector.minutes = min
        if hour > 12{
            timeSelector.hours = hour - 12
            timeSelector.isAm = false
        }else{
            timeSelector.hours = hour
            timeSelector.isAm = true
        }
        timeSelector.presentOnView(view: self.view)
    }
    
    func setLabelFromDate(_ date: Date) {
        let df = DateFormatter()
        df.dateFormat = "hh:mm a"
        if isSelect{
            etFromDate.text = "\(df.string(from: date))"
        }else{
            etEndDate.text = "\(df.string(from: date))"
        }
    }
    
    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnSave(_ sender: Any) {
        if etWeek.text == ""{
            self.view.makeToast("Select Week")
            return
        }
        
        if etFromDate.text == ""{
            self.view.makeToast("Select From Date")
            return
        }
        
        if etEndDate.text == ""{
            self.view.makeToast("Select End Date")
            return
        }
        
        mModel = OpenHoursModel(starting_time: etFromDate.text ?? "", closing_time: etEndDate.text ?? "", notes: "", weekName: etWeek.text ?? "")
        
        if mModel != nil{
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "update_opening"), object: mModel)
            self.navigationController?.popViewController(animated: true)
        }
    }
    
}
