//
//  SquareView.swift
//  Softzino_ Assignment
//
//  Created by Pritom Dutta on 1/9/21.
//

import Foundation
import UIKit

class SquareView: UIView {
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initializeView()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initializeView()
    }
    
    func initializeView() {
//        self.frame = self.bounds
        self.layer.cornerRadius = 10
        self.layer.borderColor = UIColor.clear.cgColor
        self.layer.borderWidth = 0.5
        self.layer.masksToBounds = true
//        self.addSubview(borderView)
        
        self.layer.masksToBounds = false
        self.layer.shadowRadius = 5
        self.layer.shadowOpacity = 0.3
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOffset = CGSize(width: 0 , height:1)

    }
}
